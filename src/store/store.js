import Vue from 'vue'
import Vuex from 'vuex'
import firebase from 'firebase/app'
import 'firebase/firestore'

Vue.use(Vuex)

var config = {
	apiKey: 'AIzaSyAxUXXabsTGzjyOQlTb1T55_Wq9ieniTCg',
	authDomain: 'gis-embung.firebaseapp.com',
	databaseURL: 'https://gis-embung.firebaseio.com',
	projectId: 'gis-embung',
	storageBucket: 'gis-embung.appspot.com',
	messagingSenderId: '797664606977'
}
firebase.initializeApp(config)

const settings = {}
firebase.firestore().settings(settings)

const embungsCollection = firebase.firestore().collection('embung')

export const store = new Vuex.Store({
	state: {
		user: null,
		embungs: null,
		embung: null
	},
	getters: {
		getEmbungs: state => {
			return state.embungs
		},
		getEmbung: state => {
			return state.embung
		},
		getUser: state => {
			return state.user
		}
	},
	mutations: {
		setUser(state, user) {
			state.user = user
		},
		setEmbungs: state => {
			embungsCollection
				.orderBy('no', 'asc')
				.onSnapshot((embungRef) => {
					const embungs = {}
					embungRef.forEach((doc) => {
						embungs[doc.id] = doc.data()
					})
					state.embungs = embungs
				})
		},
		setEmbung(state, id) {
			embungsCollection
				.doc(id)
				.get()
				.then(function (doc) {
					if (doc.exists) {
						state.embung = doc.data()
					} else {
						// doc.data() will be undefined in this case
						console.log("No such document!")
					}
				}).catch(function (error) {
					console.log("Error getting document:", error)
				})
		}
	},
	actions: {
		setEmbungs: context => {
			context.commit('setEmbungs')
		},
		setEmbung(context, id) {
			context.commit('setEmbung', { id })
		}
	},
	plugins: [
	]
})
