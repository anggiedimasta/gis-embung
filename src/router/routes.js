import Home from '@/components/Home'
import MapView from '@/components/Map'
import Analysis from '@/components/Analysis'
import MapService from '@/components/MapService'
import SignIn from '@/components/SignIn'
import SignUp from '@/components/SignUp'
import Error404 from '@/components/Error404'
import Dashboard from '@/components/dashboard/Dashboard'
import Data from '@/components/dashboard/Data'
import Edit from '@/components/dashboard/Edit'

// This is where you add all your site routes
// Each route is set as an obect in the array
// For a the most basic route just set
// the path & component to load

export const routes = [
	{
		display_name: 'Home',
		path: '/',
		name: 'home',
		component: Home,
		meta: {
			title: 'Home'
		}
	},
	{
		display_name: 'Map',
		path: '/map',
		name: 'map',
		component: MapView,
		meta: {
			title: 'Map'
		}
	},
	{
		display_name: 'Analysis',
		path: '/analysis',
		name: 'analysis',
		component: Analysis,
		meta: {
			title: 'Analysis'
		}
	},
	{
		display_name: 'Map Service',
		path: '/mapservice',
		name: 'mapService',
		component: MapService,
		meta: {
			title: 'Map Service'
		}
	},
	{
		display_name: 'Sign In',
		path: '/signin',
		name: 'signin',
		component: SignIn,
		meta: {
			title: 'Sign In'
		}
	},
	{
		path: '/login',
		redirect: '/signin'
	},
	{
		path: '/admin',
		redirect: '/signin'
	},
	{
		path: '/cpanel',
		redirect: '/signin'
	},
	{
		display_name: 'Sign Up',
		path: '/signup',
		name: 'signup',
		component: SignUp,
		meta: {
			title: 'Sign Up'
		}
	},
	{
		display_name: 'Dashboard',
		path: '/dashboard',
		name: 'dashboard',
		component: Dashboard,
		meta: {
			requiresAuth: true,
			title: 'Dashboard'
		}
	},
	{
		display_name: 'Data',
		path: '/dashboard/data',
		name: 'data',
		component: Data,
		meta: {
			requiresAuth: true,
			title: 'Data'
		}
	},
	{
		display_name: 'Edit',
		path: '/dashboard/edit/:id',
		name: 'edit',
		component: Edit,
		props: true,
		meta: {
			requiresAuth: true,
			title: 'Edit'
		}
	},
	{
		display_name: '404',
		path: '/404',
		name: '404',
		component: Error404,
		meta: {
			title: '404'
		}
	},
	{
		path: '*',
		redirect: '/404'
	}
]
