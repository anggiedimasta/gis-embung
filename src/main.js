// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import firebase from 'firebase/app'
import 'firebase/auth'

import { store } from './store/store'
import { routes } from './router/routes'

Vue.config.productionTip = false

Vue.use(VueRouter)

const router = new VueRouter({
	routes: routes,
	mode: 'history'
})

router.beforeEach((to, from, next) => {
	const currentUser = store.getters.getUser
	const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
	document.title = to.meta.title + ' | GIS Embung Flores'
	if (requiresAuth && !currentUser) {
		next('/signin')
	} else if (!requiresAuth && currentUser) {
		next()
	} else if (requiresAuth && currentUser) {
		next()
	} else {
		next()
	}
})

new Vue({
	el: '#app',
	router: router,
	store: store,
	render: h => h(App),
	created: function () {
		firebase.auth().onAuthStateChanged((user) => {
			this.$store.commit('setUser', user)
		})
	}
})
